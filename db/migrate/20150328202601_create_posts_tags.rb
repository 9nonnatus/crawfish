class CreatePostsTags < ActiveRecord::Migration
  def change
    create_table :posts_tags, id: false do |t|
      t.references :post, null: false
      t.references :tag, null: false
    end
  end
end
