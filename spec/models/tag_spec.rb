require 'rails_helper'
require 'shoulda-matchers'

describe Tag do
  describe 'associations' do
    it { should have_and_belong_to_many :posts }
  end

  describe 'validations' do
    it { should validate_presence_of :name }
    it { should validate_uniqueness_of(:name).case_insensitive }
  end

  it 'has a valid factory' do
    expect(build :tag).to be_valid
  end
end
