require 'rails_helper'
require 'shoulda-matchers'

describe User do
  describe 'associations' do
    it { should have_many :posts }
  end

  describe 'validations' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :slug }
    it { should validate_presence_of :email }
    it { should validate_presence_of :password_digest }
    it { should validate_uniqueness_of(:name).case_insensitive }
    it { should validate_uniqueness_of(:slug).case_insensitive }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end

  it { should have_secure_password }

  it 'has a valid factory' do
    expect(build :user).to be_valid
  end
end
