require 'rails_helper'
require 'shoulda-matchers'

describe Post do
  describe 'associations' do
    it { should belong_to :user }
    it { should have_and_belong_to_many :tags }
  end

  describe 'validations' do
    it { should validate_presence_of :user }
    it { should validate_presence_of :title }
    it { should validate_presence_of :slug }
    it { should validate_presence_of :body }
    it { should validate_presence_of :published_at }
    it { should validate_uniqueness_of(:slug).case_insensitive }
  end

  it 'has a valid factory' do
    expect(build :post).to be_valid
  end

  describe 'default scope' do
    it 'sorts posts reverse-chronologically by time published' do
      middle_post = create :post, published_at: 12.hours.ago
      new_post = create :post, published_at: DateTime.current
      old_post = create :post, published_at: 1.day.ago
      expect(Post.all).to eq [new_post, middle_post, old_post]
    end

    it 'excludes unpublished posts' do
      published_post = create :post
      unpublished_post = create :post, published: false
      expect(Post.all).to eq [published_post]
    end

    it 'excludes postdated posts' do
      postdated_post = create :post, published_at: 1.day.from_now
      post = create :post
      expect(Post.all).to eq [post]
    end
  end

  it 'automatically sets published to true' do
    expect(Post.new.published?).to eq true
  end

  describe '#admin_view' do
    it 'includes unpublished posts' do
      unpublished_post = create :post, published: false
      published_post = create :post
      expect(Post.admin_view).to match_array [unpublished_post, published_post]
    end

    it 'includes postdated posts' do
      postdated_post = create :post, published_at: 1.day.from_now
      expect(Post.admin_view).to match_array [postdated_post]
    end
  end
end
