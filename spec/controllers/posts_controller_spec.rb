require 'rails_helper'

shared_examples 'requires logged in' do
  before { session[:user_id] = nil }

  it 'sets a flash error to "You are not logged in."' do
    action
    expect(flash[:error]).to eq 'You are not logged in.'
  end

  it 'redirects to the login path' do
    action
    expect(response).to redirect_to root_path
  end
end

describe PostsController do
  before { session[:user_id] = create(:user).id }

  describe 'GET #index' do
    it 'sets @posts to all posts' do
      get :index
      expect(assigns :posts).to eq Post.all
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #new' do
    it_behaves_like 'requires logged in' do
      let(:action) { get :new }
    end

    it 'sets @post to a new post' do
      get :new
      expect(assigns :post).to be_a_new Post
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template :new
    end
  end
end
