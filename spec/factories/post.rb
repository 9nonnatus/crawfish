FactoryGirl.define do
  factory :post do
    title { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraphs(6).join("\n\n") }
    published_at { DateTime.current }
    published true
    user
  end
end
