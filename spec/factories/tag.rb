FactoryGirl.define do
  factory :tag do
    sequence(:name) { |n| n.to_s }
  end
end
