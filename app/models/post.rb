class Post < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  belongs_to :user
  has_and_belongs_to_many :tags

  validates_presence_of :user
  validates_presence_of :title
  validates_presence_of :slug
  validates_presence_of :body
  validates_presence_of :published_at
  validates_uniqueness_of :slug, case_sensitive: false

  default_scope -> { current.reverse_chronological }

  scope :reverse_chronological, -> { order 'published_at DESC' }

  scope :published, -> { where published: true }

  scope :current, -> { published.where ['published_at <= ?', DateTime.current] }

  scope :admin_view, -> { unscope(:where) }
end
