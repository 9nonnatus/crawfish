class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_secure_password

  has_many :posts

  validates_presence_of :name
  validates_presence_of :slug
  validates_presence_of :email
  validates_presence_of :password_digest
  validates_uniqueness_of :name, case_sensitive: false
  validates_uniqueness_of :slug, case_sensitive: false
  validates_uniqueness_of :email, case_sensitive: false
end
